# Yume QA词库
> ## 一.[特定名词解释](#一-特定名词解释)
> -    [词库回复的优先级及索引说明](#词库回复的优先级及索引顺序说明由上到下判定)
> -    [复读模式](#复读模式)
> -    [精确](#精确)
> -    [模糊](#模糊)
> -    [设置复读概率](#复读概率)
> -    [设置词库概率](#词库概率)
> ## 二.[下载词库](#二-下载词库)
> ## 三.[添加/删除/修改/查询问答](#三-添加问答)
> ## 四.[删除问答](#四-删除问答)
> ## 五.[查询问答](#五-查询问答)
> ## 六.[本群词库](#六-本群词库)
> ## 七.[更新日志](#七-更新日志)




# 一. 特定名词解释
>- ## 词库回复的优先级及索引顺序说明(由上到下判定)
>   - #### 优先级：  [精确类](#精确) > [复读类](#复读) > [模糊类](#模糊)
>       - Step.1. 进入[精确类](#精确)，为固定触发，无概率
>       - Step.2. 根据[词库重置时间](#词库重置时间)去除[精确类](#精确)的词库重置时间内的已发送词条
>       <br/>重置时间内剩余可用词条=0：到Step.3
>       <br/>重置时间内剩余可用词条>0：到Step.7
>       - Step.3. 进入[复读类](#复读)
>       - Step.4. 根据[复读次数](#复读次数)过滤 ,即 未达到 复读次数 则跳过复读类进入模糊类 Step.7 反之执行 Step.5
>       <br/>本群复读次数!=设定次数：到Step.7
>       <br/>本群复读次数≥ 设定次数：到Step.5
>       - Step.5. 根据[复读概率](#复读概率)过滤 ,即 未达到 一定概率 则跳过复读类进入模糊类 Step.7 反之执行 Step.6
>       <br/>复读概率 ≤ 100内随机数：到Step.7
>       <br/>复读概率 > 100内随机数：到Step.6
>       - Step.6. 根据[复读重置时间](#复读重置时间)去除[复读类](#复读)的词库重置时间内的已发送词条
>       <br/>重置时间内剩余可用词条=0：到Step.7
>       <br/>重置时间内剩余可用词条>0：到Step.10
>       - Step.7. 进入[模糊类](#模糊)
>       - Step.8. 根据[词库概率](#模糊概率)过滤 ,即 未达到 一定概率 则跳过模糊类到结束 Step.0，反之执行 Step.9
>       - Step.9. 根据[词库重置时间](#复读重置时间)去除[模糊类](#模糊)的词库重置时间内的已发送词条
>       <br/>重置时间内剩余可用词条=0：到Step.0
>       <br/>重置时间内剩余可用词条>0：到Step.10
>       - Step.10. 发送消息
>       - Step.0. 结束

>   -   ## 复读
>       - ### 词库中的复读类(5)
>       - 位置：[Step.3](#优先级--精确类--复读类--模糊类) 即非固定一段文字模糊触发
>       -   ### /设置复读模式 [[复读模式](#1-复读模式)] [[复读次数](#2-复读次数)] [[复读概率](#3-复读概率)] [[复读重置时间](#4-复读重置时间)]
>           需群管理以上权限，例： /设置复读模式 3 0 80 300
>          <br/> 推荐：/设置复读模式 1 0 100 300   <br/>先熟悉一下机器人操作
>       -   ### 1. 复读模式
>           -   词库中的复读类(5),优先级排第二
>           <br/>BOT新特性中支持了[自主学习回复](#复读模式)的功能
>           <br/>不单纯是复读, 而是记录一条发言的前两条消息(并进行过滤) 
>           <br/>以达到自动承接上下文比较强硬的语言学习
>           <br/>考虑到每个群的功能性不同,涉及到各群隐私,默认为不开启状态
>           - 模式0. 默认为0,不存储数据,不回复
>           - 模式1. 复读本群(纯文字)
>           - 模式2. 复读本群(纯图片)
>           - 模式3. 复读本群(all)
>           - 模式4. 复读所有群(纯文字)
>           - 模式5. 复读所有群(纯图片)
>           - 模式6. 复读所有群(all)
>           - 模式7. 复读所有群(纯文字) 跳过[Step.6](#优先级--精确类--复读类--模糊类)
>           - 模式8. 复读所有群(纯图片) 跳过[Step.6](#优先级--精确类--复读类--模糊类)
>           - 模式9. 复读所有群(all)   跳过[Step.6](#优先级--精确类--复读类--模糊类)
>       -   ### 2. 复读次数
>           - 位置：[Step.4](#优先级--精确类--复读类--模糊类)
>           - 范围：0-2
>               - #### 需要权限： [Alv.1+](#3alv管理权限) 或 群管理
>               - 例： /设置复读模式 x [0](#3-复读概率) xx xxx
>       -   ### 3. 复读概率
>           - 位置：[Step.5](#优先级--精确类--复读类--模糊类)
>           - 范围：0-100
>               - #### 需要权限： [Alv.1+](#3alv管理权限) 或 群管理
>               - 例： /设置复读模式 x x [25](#3-复读概率) xxx
>               - 当复读模式>0时进行复读库索引,触发概率默认为25%
>       -   ### 4. 复读重置时间
>           - 位置：[Step.6](#优先级--精确类--复读类--模糊类)
>           - 范围：0-无限
>               - #### 需要权限： [Alv.1+](#3alv管理权限) 或 群管理
>               - 例： /设置复读模式 x x xx [300](#4-复读重置时间)
>       -   ### 5. 最终效果：
>       - ![示例1](https://images.gitee.com/uploads/images/2020/1012/171553_c51c2c2e_2223710.png "示例1.png")
>       - ![示例2](https://images.gitee.com/uploads/images/2020/1012/171646_4b5af78a_2223710.png "示例2.png")



>- ## 精确
>   - ### 词库中的精确类(2,4)
>   - 位置：[Step.1](#优先级--精确类--复读类--模糊类) 即固定词汇触发,优先于所有词库类

>- ## 模糊
>   - ### 词库中的模糊类(1,3)
>   - 位置：[Step.7](#优先级--精确类--复读类--模糊类) 即非固定一段文字模糊触发
>   - 一段文字包含有多个触发词条也是随机触发
>   - ### 词库概率
>       - #### 需要权限： [Alv.1+](#3alv管理权限) 或 群管理
>       - 例： /设置词库概率25
>       - 即模糊概率, 优先级最低层 
>       - [模糊](#模糊) 触发概率默认为30%
>   - ### 词库重置时间
>       - 位置：[Step.2 & Step.8](#优先级--精确类--复读类--模糊类)
>       - 范围：0-无限
>       - #### 需要权限： [Alv.1+](#3alv管理权限) 或 群管理
>       - 例： /设置词库重置时间 200 (#词库重置时间)
>- ## 全局与非全局
>   - 即 所有群 或 本群



>- ## 库(form)
>   - #### 此为固定的全局云词库,以编号整合,方便后续直接按编号下载
>   - 只有Alv.6+才有权限编辑库
>   - Alv.1+ 可下载库(删除不会删除云部分)
>   - 第二次下载即更新库
>   - 目前 setu库为4<br/>包含 setu,色图,铜图,喵图
>   - 下载例： /[下载词库4](#二-下载词库) 

> ## [ALV](https://gitee.com/r1e/ym/blob/1/qd%E7%AD%BE%E5%88%B0.md#alv)(管理权限)
>   - 本社问答需要Alv--管理权限>0 才可使用   <br/>即配合签到插件的 '/setalv'  (设置管理权限)进行使用
>   -   #### 需要Admin权限，例：/setalv qq号/@qq 1<br/>/setalv 123456 1
>   - ### Alv可使用词库权限说明
>       - ### 云端
>           - Alv.7. 所有权限 下载/增删库权限  
>           - Alv.6. 所有权限 下载/增删库权限 
>           - Alv.5. 添加/删除 云全局词库的权限 下载库权限
>           - Alv.4. 添加/删除 云非全局词库的权限 下载库权限
>       - ### 本地插件端
>           - Alv.3. 所有权限 下载库权限 添加/删除 云词库权限
>           - Alv.2. 添加/删除 全局词库的权限
>           - Alv.1. 添加/删除 非全局词库的权限
>           - Alv.0. 没有权限添加词库(防止发色图被封号)


## 二. 下载词库
这个是Yume自己添加的词库  1-10为类别
<br/>目前可知：4为图片类(setu)  
>- /下载词库4 
><br/>需要alv.1 +权限
>- /更新本群词库
><br/>需要alv.1 +权限
>- /更新所有群词库
><br/>需要Admin权限

## 三. 添加问答

### 指令
>- 学习问xx答xx
<br />全局模糊
>- 学习精确问xx答xx
<br />全局精确
>- 添加问xx答xx
<br />本群模糊
>- 精确问xx答xx
<br />本群精确


## 四. 删除问答
>- 删除id
>- 删除问xx答xx

## 五. 查询问答
>- 查询id
>- 查询问
<br/>例：/查询问setu
>- 查询答
<br/>例：/查询答(bot发过的消息)
>- 查询库N
<br/>例：/查询库4

## 六. 本群词库
>- /本群词库
>- /查询本群词库


## 七. 更新日志
>- 2020.10.2 引入"重置时间"概念,分为"复读重置时间"和"词库重置时间"
>- 2020.10.15 屏蔽某人的qq 防止多个bot触发 /添加词库屏蔽 qq号 qq号 qq号 
>- 2020.10.15 段子<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/1015/114102_c81db2ee_2223710.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1015/114110_d9926a21_2223710.png "屏幕截图.png")