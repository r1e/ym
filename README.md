## YumeBot
#### 目前使用的是先驱框架 (这个框架目前是免费的，虽然作者还在迭代开发中，不确定bug一堆。不过各种程度上也算是很慷慨了)
[toc]
### 环境：先驱ver.2020090401 v3 
还是在开发中的内测sdk，所以不确定之后会不会有大改。
### 框架使用说明
1. 打开先驱 在[账号管理]列表添加账号，这里推荐手机端扫码登录
2. 点击[插件扩展] 右键启动 [AaYumePlugin]
这里会弹出本框架登录框，首次需输入botQQ和AdminQQ，默认核对第一个qq，请确认[账号管理]列表最上面是你填的botQQ
3. 核验登录成功后，点击启动插件。 
4. 付费插件请根据插件列表的 [N] 在平台购买月卡，并按相应步骤进行激活。
5. 如果bot机器环境有所变动，可点击 [同步到期时间] 同步月卡

## 插件使用说明(部分)
#### 以下说明将会以 '指令' 作标题，其中 ''内的'/' 为指令符，
#### 请注意，为了更好地优化，减少索引判断次数，大多数指令是需要加'/'的，具体看'指令'里是否出现'/'
#### 本框架各插件的 配置.ini 中 指令符不手动设置的话 默认为'/' 

## 一. [5][QD签到](https://gitee.com/r1e/ym/blob/1/qd%E7%AD%BE%E5%88%B0.md) yume.sup.0rei
### 请转到[QD签到](https://gitee.com/r1e/ym/blob/1/qd%E7%AD%BE%E5%88%B0.md)查看

## 二. [0][召唤开关] yume.call.0rei
1. '召唤' / '@Bot 召唤'
2. '去面壁' / '@Bot 去面壁'
3. 点击 菜单 可以自定义设置 召唤 / 去面壁的 指令

## 三. [5][订阅小助手](https://gitee.com/r1e/ym/blob/1/%E8%AE%A2%E9%98%85%E5%B0%8F%E5%8A%A9%E6%89%8B.md) yume.twisub.0rei
### 请转到[订阅小助手](https://gitee.com/r1e/ym/blob/1/%E8%AE%A2%E9%98%85%E5%B0%8F%E5%8A%A9%E6%89%8B.md)查看

## 四. [6][QA词库](https://gitee.com/r1e/ym/blob/1/qa.md) yume.qa.0rei
### 请转到[QA词库](https://gitee.com/r1e/ym/blob/1/qa.md)查看

## 五. [6][自定义抽卡](https://gitee.com/r1e/ym/blob/1/%E8%87%AA%E5%AE%9A%E4%B9%89%E6%8A%BD%E5%8D%A1.md) yume.card.0rei
### 请转到[自定义抽卡](https://gitee.com/r1e/ym/blob/1/%E8%87%AA%E5%AE%9A%E4%B9%89%E6%8A%BD%E5%8D%A1.md)查看

## 六. [4][Pixiv搜图](https://gitee.com/r1e/ym/blob/1/Pixiv%E6%90%9C%E5%9B%BE.md) yume.pixiv.0rei
### 请转到[Pixiv搜图](https://gitee.com/r1e/ym/blob/1/Pixiv%E6%90%9C%E5%9B%BE.md)查看
