
# 目前只支持twi，后续会增加更多
### 注意,本插件需要设置代理vpn,并在配置.ini中设置 vpnport 配合使用(http代理端口号)
### 推荐[下载使用qv2ray](https://gitee.com/r1e/qv2r)
errcode:326 账号被暂时冻结，需要上线进行Google认证

# 一. 订阅
## 首次使用需要设置时间间隔
/t设置 /twi设 /tset + timing 1
设定一个推送时间间隔，Admin权限
#### 注意！，设定间隔这个很重要，如果设定推送过于频繁很有可能冻号，请Admin自己合理设定，酌情考虑。
#### 这里为详细说明，方便理解后面的推荐指令，自己一条一条设定吧。
### 本社的可设推特订阅目前分为4类： home po rt like 
例:
>- /tset timing home 30
<br />设定home(主页推荐) 的推送间隔为 30 分钟
>- /twisettiming po 0
<br />设定po(发推) 的推送间隔为 0 分钟,即 关闭，不推送
>- /tsettiming   rt 20
<br />设定rt(转推) 的推送间隔为 20 分钟
>- /tsettiming like 1
<br />设定like(我的点赞) 推送间隔为 1 分钟

## 然后订阅
>- /t订阅 po + uid <br />(用户的推特id) 
>- /t订阅 rt + uid <br />(用户的推特id) 
>- /t订阅 like + uid <br />(用户的推特id)
 需要私聊/setcookie。
>- /t订阅 home  <br />订阅自己的home(主页推荐)
 需要私聊/setcookie。




# 二. /看,/see,/kan,/twi,/t 指令前缀
- 普通前缀可触发要求区别不高的指令。
- /t和/twi 特指 twi系列功能的前缀，以下看到即此两者都可触发
# 三. /t settwicookie+你的twicookie 
#### 因为twi的特殊性，部分twi的功能需要使用到cookie。具体视情况而定，订阅会有提示的
#### 如何获取cookie，请自行百度或问群友，注意！请不要把cookie交给任何其他人，最好也不要发群里。
#### 所以这里推荐 
>- /settwicookie你的twicookie  <br />私聊bot使用 
# 四. /看+twi链接
/看 链接 支持twi 短链,长链,tid 
> ### /twi t.co/lbHOwe7Ehh
> ### /kan m 4 t.co/lbHOwe7Ehh
> ### /see 1303142677895020544
> ### /t m 1 https://twitter.com/gagaimo_man/status/1307295034560012288

#### m为mode mode分为9个模式 不带m或0为普通,1为链接,2为。链接，3为高斯模糊5% 3以上 为高斯模糊++
# 五. /本群订阅
- 查看本群的订阅 和 各个分类订阅的时间间隔
![本群订阅](https://i0.hdslb.com/bfs/album/2d46a10377e3feea55ae964f0ff669fc07c2951b.png "本群订阅")

# 六. /tset不使用slv
- 仅Admin可使用
- 全局开关slv,默认0,关闭
- 开启则不区分敏感图片
- 关闭则依照原先设定,区分敏感度
# 七. /t设置 /twi设 /tset + slv
- 设定一个slv，Admin权限--Alv
- 权限模式0.默认只有admin可用 
- 权限模式1.Alv.1以上可用， < Alv.2则不可改他群数据 
- 权限模式2.无权限，但非admin不可改别群数据
例：/tsetslv  sid/cid/uid(uid时不用带u) 1-9
# 八. /t设置 /twi设 /tset + mod
- 设定一个mode，Admin权限
- 权限模式0.默认只有admin可用 
- 权限模式1.Alv.1以上可用， < Alv.2则不可改他群数据 
- 权限模式2.无权限，但非admin不可改别群数据
例：/tsetmode sid/cid/uid(uid时不用带u) 1-9
# 九. /t设置 /twi设 /tset + 昵称
- 设定一个昵称，Admin权限
- 权限模式0.默认只有admin可用 
- 权限模式1.Alv.1以上可用， < Alv.2则不可改他群数据 
- 权限模式2.无权限，但非admin不可改别群数据
例：/tset昵称 sid/cid/uid(uid时不用带u) +你想设定的昵称
# 十. /t设置 /twi设 /tset + 开关
 设定一个开关，Admin权限
例：/tset开关 sid/cid/uid(uid时不用带u) 1
- 0为开放 1为关闭
- 权限模式0.默认只有admin可用 
- 权限模式1.Alv.1以上可用， < Alv.2则不可改他群数据 
- 权限模式2.无权限，但非admin不可改别群数据


## 以上为参照。现在为指令教程
## 比如你现在想订阅 推送 自己的点赞

>- /本群订阅
>- /tset timing like 3
>- /t订阅 like twiuid 
>- /本群订阅
>- /tset mod s1 1

1. /本群订阅                                    查看你想订阅的目标是否有在本群订阅中
2. /tset timing like 3                         设置推送时间间隔为3分钟
3. /t订阅 like 自己的推特uid                    订阅 like userid 
4. /本群订阅                                    这一步本群订阅为了查看mode id
5. /tset mod 1                                 设置这条订阅的收图模式



- 哼哼啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊

## slv 参照
- 0. 普通发图(遇到涩图转成链接)
- 1. 直接图改链
- 2. 图改。链
- 3. 显示涩图
- 4. 高斯模糊25%
- 5. 高斯模糊45%
- 6. 秀图0
- 7. 秀图1幻影
- 8. 秀图2抖动
- 9. 秀图3生日
- 10. 秀图4爱你
- 11. 秀图5征友
- 12. 秀图6随机
- 13. block
- 
## mode 参照
- 0. 普通发图
- 1. 制作单图,暂不开放
- 2. po
- 3. po 仅显示短链+图片 
- 4. po 仅显示图片
- 5. rt 
- 6. rt 仅显示短链+图片
- 7. rt 仅显示图片
- 8. all
- 9. all 仅显示短链+图片
- 10. all 仅显示图片
- 11. 纯文章
- 12. block







