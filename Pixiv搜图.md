> # "哪个是规范都不知道，真是祖宗都不要了"  
> # 真心希望某煞笔祖宗没了 2020.10.8

## Pixiv系列插件，支持排榜，搜图，搜番，搜作者，搜关键词 并简化指令
>- #### [一.Pixiv排榜](#一-Pixiv排榜)
>- #### [二.Pu 排版UserId 图片](#二-Pu)
>- #### [三.Pv 查看PixivID图片](#三-Pv)
>- #### [四.搜图](#四-搜图)
>- #### [五.搜番](#五-搜番)
# 一. Pixiv排榜
>- ### 发送指令会对相应的版块进行排版(支持显示Unicode)
>    - P站日榜 pd
>    - P站周榜 pw 
>    - P站月榜 pm
>    - p站新人榜 pn 
>    - p站原创榜 po
>    - p站男性向 pb   
>    - p站p站女性向 pb 
>- ### 示例
>    - [/pw](#pw图片示例) 
>    - [/P站日榜](#pw图片示例) 
>    - [/pmp=2](#pw图片示例)
>- ### [点击移动到pw图片示例](#pw图片示例)

# 二. Pu
>- ### 根据P站作者的UserID排版最新的N张图片
>- 使用例:
>- /pu UserID
![/pu 1113943](https://images.gitee.com/uploads/images/2020/1007/193707_04663e25_2223710.png "/pu1113943")

# 三. Pv 
>- ### 根据 PixivID 查看图片，如果事先已经使用过排榜，则可以使用 "/pv 短码" 同理查看 PixivID 的图片
>- 如果该PixivID集里包含有多张图片，则可以使用 p=2 或 + [n]-[n+] 等形式查看第一张之外的多个图片，例：
>    - /pv PixivID p=2
>    - /pv PixivID +
>    - /pv PixivID 2-5
>- 如果设定 配置.ini 中 [pv是否默认原图] 为1,则pv 显示为原图(默认为0:非原图)
>- 使用例:
>- /pvb uid
>- /pvb 短码
>- ![/pvb 82693472](https://images.gitee.com/uploads/images/2020/1007/162420_d7297594_2223710.png "/pvb 82693472")
>- ![/pv](http://ae01.alicdn.com/kf/H69b7d7270e3740fbb731ae363d37e8ddb.jpg "/pmp=2")
>- ![/pvb r57](https://images.gitee.com/uploads/images/2020/1007/160329_aa0516d4_2223710.png "/pv")
>- ![/pvb m23 +](https://images.gitee.com/uploads/images/2020/1007/164942_6ec9efc7_2223710.png "/pvb m23 +")

# 四. 搜图
>- 使用saucenao搜索Pixiv范围内的图片
>- 使用例:/st[图片]
>- ![/st[图片]](https://images.gitee.com/uploads/images/2020/1008/065734_1b1bd737_2223710.png "/st[图片]")

# 五. 搜番
>- 搜索番剧，注:仅限相对完整且比例完整番剧出现过的图片
>- 使用例:/sf[图片]
![/sf[图片]](https://images.gitee.com/uploads/images/2020/1007/183938_cd13d6d1_2223710.png "/sf[图片]")

# /pw图片示例
>- ![/pw](https://images.gitee.com/uploads/images/2020/1007/165252_cca4592e_2223710.png "/pw")
>- ![/pmp=2](https://images.gitee.com/uploads/images/2020/1007/165147_192b7071_2223710.png "/pmp=2")
>- ![/pmp=2](https://ae01.alicdn.com/kf/H0fdf1b561a484f6f8bc2bfce7c3352d6A.jpg "/pmp=2")